import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';

import Header from './components/Header.vue';
import Home from './routes/Home.vue';

Vue.use(VueRouter);
Vue.config.productionTip = false;

Vue.component('Header', Header);

const router = new VueRouter({
  routes: [
    { name: 'Home', path: '/', component: Home },
  ],
  mode: 'history',
});

new Vue({
  el: '#app',
  router,
  render: h => h(App),
});
