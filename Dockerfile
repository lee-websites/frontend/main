LABEL author="Leon Kappes"
# <----------------------- Builder ----------------------->
FROM node:lts-alpine AS builder

WORKDIR /files

RUN npm install
RUN npm run build

# <----------------------- RUNNER ----------------------->

FROM nginx:alpine

RUN apk add --no-cache ca-certificates

WORKDIR /app
COPY --from=builder /files/build /app/build
